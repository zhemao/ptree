#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include "ptree.h"

#define SYSCALL_PTREE 223

static void print_indent(int n)
{
	int i;
	for (i = 0; i < n; i++)
		printf("\t");
}

static void print_entry(const struct prinfo *entry)
{
	printf("%s,%d,%ld,%d,%d,%d,%ld\n", entry->comm, entry->pid,
		entry->state, entry->parent_pid, entry->first_child_pid,
		entry->next_sibling_pid, entry->uid);
}

static void print_entries(const struct prinfo *entries, const int num_copied)
{
	int i, indent = 0, stack = 0;
	for (i = 1; i < num_copied; i++) {
		const struct prinfo *cur_entry = &entries[i];

		if (cur_entry->next_sibling_pid == 0 && i != 0)
			stack++;

		print_indent(indent);
		print_entry(cur_entry);

		if (cur_entry->first_child_pid != 0)
			indent++;
		else if (cur_entry->next_sibling_pid == 0 && stack > 0) {
			indent -= stack;
			stack = 0;
		}
	}
}

int main()
{
	struct prinfo buf;

	/* Get the number of entries so that we can allocate enough space. */
	int num_copied = 1;
	int num_entries = syscall(SYSCALL_PTREE, &buf, &num_copied);
	if (num_entries < 0) {
		perror("error");
		exit(EXIT_FAILURE);
	}

	printf("%d\n", num_entries);

	struct prinfo *entries = (struct prinfo *)
		malloc(num_entries * sizeof(struct prinfo));

	/* TODO: Beware the Heisenbug. Number of entries might change. */
	num_copied = num_entries;
	num_entries = syscall(SYSCALL_PTREE, entries, &num_copied);

	if (num_entries < 0) {
		perror("error");
		exit(EXIT_FAILURE);
	}

	printf("There are %d entries.\n", num_copied);
	printf("Copied %d entries.\n", num_entries);

	print_entries(entries, num_copied);

	exit(EXIT_SUCCESS);
}
